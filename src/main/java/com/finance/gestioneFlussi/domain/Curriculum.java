package com.finance.gestioneFlussi.domain;

import javax.persistence.*;

@Entity
@Table(name = "curriculum")
public class Curriculum {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "id_persona")
	private Long idPersona;

	@Column(name = "professione")
	private String professione;

	@ManyToOne
	private FileCaricato fileCaricato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getProfessione() {
		return professione;
	}

	public void setProfessione(String professione) {
		this.professione = professione;
	}

	public FileCaricato getFileCaricato() {
		return fileCaricato;
	}

	public void setFileCaricato(FileCaricato fileCaricato) {
		this.fileCaricato = fileCaricato;
	}
}
