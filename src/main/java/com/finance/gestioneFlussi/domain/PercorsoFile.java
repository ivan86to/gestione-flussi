package com.finance.gestioneFlussi.domain;

import javax.persistence.*;

@Entity
@Table(name = "percorsi_file")
public class PercorsoFile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "tipo_file")
	private String tipoFile;

	@Column(name = "percorso")
	private String percorso;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoFile() {
		return tipoFile;
	}

	public void setTipoFile(String tipoFile) {
		this.tipoFile = tipoFile;
	}

	public String getPercorso() {
		return percorso;
	}

	public void setPercorso(String percorso) {
		this.percorso = percorso;
	}
}
