package com.finance.gestioneFlussi.domain;


import javax.persistence.*;


@Entity
@Table(name = "indirizzi")
public class Indirizzo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "id_esterno")
	private Long idEsterno;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "cap")
	private String cap;

	@Column(name = "citta")
	private String citta;

	@ManyToOne
	private FileCaricato fileCaricato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEsterno() {
		return idEsterno;
	}

	public void setIdEsterno(Long idEsterno) {
		this.idEsterno = idEsterno;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public FileCaricato getFileCaricato() {
		return fileCaricato;
	}

	public void setFileCaricato(FileCaricato fileCaricato) {
		this.fileCaricato = fileCaricato;
	}
}
