package com.finance.gestioneFlussi.domain;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "tab_prova")
public class MyEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	
	@Column(name = "colonna1")
	private String colonna1;
	
	@Column(name = "colonna2")
	private String colonna2;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getColonna1() {
		return colonna1;
	}
	
	public void setColonna1(String colonna1) {
		this.colonna1 = colonna1;
	}
	
	public String getColonna2() {
		return colonna2;
	}
	
	public void setColonna2(String colonna2) {
		this.colonna2 = colonna2;
	}
}
