package com.finance.gestioneFlussi.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "file_caricati")
public class FileCaricato {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nome_file")
	private String nomeFile;

	@Column(name = "data_caricamento")
	private Instant dataCaricamento;

	@ManyToOne
	private PercorsoFile percorsoFile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeFile() {
		return nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public Instant getDataCaricamento() {
		return dataCaricamento;
	}

	public void setDataCaricamento(Instant dataCaricamento) {
		this.dataCaricamento = dataCaricamento;
	}

	public PercorsoFile getPercorsoFile() {
		return percorsoFile;
	}

	public void setPercorsoFile(PercorsoFile percorsoFile) {
		this.percorsoFile = percorsoFile;
	}
}
