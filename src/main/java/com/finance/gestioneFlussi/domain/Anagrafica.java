package com.finance.gestioneFlussi.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "anagrafiche")
public class Anagrafica {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "id_esterno")
	private Long idEsterno;

	@Column(name = "nome")
	private String nome;

	@Column(name = "cognome")
	private String cognome;

	@Column(name = "codice_fiscale")
	private String codiceFiscale;

	@Column(name = "data_nascita")
	private Date dataNascita;

	@ManyToOne
	private FileCaricato fileCaricato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEsterno() {
		return idEsterno;
	}

	public void setIdEsterno(Long idEsterno) {
		this.idEsterno = idEsterno;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public FileCaricato getFileCaricato() {
		return fileCaricato;
	}

	public void setFileCaricato(FileCaricato fileCaricato) {
		this.fileCaricato = fileCaricato;
	}
}
