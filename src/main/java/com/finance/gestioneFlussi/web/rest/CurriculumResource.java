package com.finance.gestioneFlussi.web.rest;


import com.finance.gestioneFlussi.service.CurriculumService;
import com.finance.gestioneFlussi.service.dto.CurriculumDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@RestController
public class CurriculumResource implements Serializable {

	private final String URL = "/api/curriculum";

	@Autowired
	private CurriculumService curriculumService;

	@PostMapping(URL)
	public List<CurriculumDTO> fileProfessioni() throws Exception {
		return curriculumService.loadCurriculumFromFile();
	}

	@GetMapping(URL + "/{id}")
	public CurriculumDTO findByID(@PathVariable Long id) {
		return curriculumService.getDTOByID(id);
	}

	@GetMapping(URL)
	public List<CurriculumDTO> getAllFileProfessioni() {
		return curriculumService.listaDTO();
	}

	@DeleteMapping(URL + "/{id}")
	public void deleteByID(@PathVariable Long id) {
		curriculumService.deleteByID(id);
	}

	@PatchMapping(URL)
	public CurriculumDTO patchedDTO(@RequestParam MultipartFile file) throws Exception {
		return curriculumService.getDTOByFile(file);
	}
}
