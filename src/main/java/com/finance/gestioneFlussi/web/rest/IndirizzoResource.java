package com.finance.gestioneFlussi.web.rest;


import com.finance.gestioneFlussi.service.IndirizzoService;
import com.finance.gestioneFlussi.service.dto.IndirizzoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@RestController
public class IndirizzoResource implements Serializable {

	private final String URL = "/api/indirizzo";

	@Autowired
	IndirizzoService indirizzoService;

	@PostMapping(URL)
	public List<IndirizzoDTO> saveFile() throws Exception {
		return indirizzoService.uploadIndirizziFromFile();
	}

	@GetMapping(URL)
	public List<IndirizzoDTO> getAllIndirizziDTO() {
		return indirizzoService.listaDTO();
	}

	@GetMapping(URL + "/{id}")
	public IndirizzoDTO getById(@PathVariable Long id) {
		return indirizzoService.getDTOByID(id);
	}

	@DeleteMapping(URL + "/{id}")
	public void deleteByID(@PathVariable Long id) {
		indirizzoService.delDTO(id);
	}

	@PatchMapping(URL)
	public IndirizzoDTO savePatch(MultipartFile file) throws Exception {
		return indirizzoService.patchIndirizziFromFile(file);
	}

}
