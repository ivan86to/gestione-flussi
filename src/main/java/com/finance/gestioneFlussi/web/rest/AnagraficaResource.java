package com.finance.gestioneFlussi.web.rest;

import com.finance.gestioneFlussi.service.AnagraficaService;
import com.finance.gestioneFlussi.service.dto.AnagraficaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@RestController
public class AnagraficaResource implements Serializable {

	private final String URL = "/api/anagrafica";

	@Autowired
	private AnagraficaService anagraficaService;

	@PostMapping(URL)
	public List<AnagraficaDTO> caricaFile() throws Exception {
		return anagraficaService.caricaFile();
	}

	@GetMapping(URL)
	public List<AnagraficaDTO> getAllFileAnagrafici() {
		return anagraficaService.listaDTO();
	}

	@GetMapping(URL + "/{id}")
	public AnagraficaDTO findByID(@PathVariable Long id) {
		return anagraficaService.getDTOByID(id);
	}

	@DeleteMapping(URL + "/{id}")
	public void deleteById(@PathVariable Long id) {
		anagraficaService.deleteById(id);
	}

	@PatchMapping(URL)
	public AnagraficaDTO uploadFile(@RequestParam MultipartFile file) throws Exception {
		return anagraficaService.uploadFile(file);
	}

}
