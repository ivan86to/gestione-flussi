package com.finance.gestioneFlussi.repository;

import com.finance.gestioneFlussi.domain.Anagrafica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnagraficaRepository extends JpaRepository<Anagrafica, Long> {

}
