package com.finance.gestioneFlussi.repository;

import com.finance.gestioneFlussi.domain.FileCaricato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileCaricatoRepository extends JpaRepository<FileCaricato, Long> {

}
