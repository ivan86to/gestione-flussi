package com.finance.gestioneFlussi.repository;

import com.finance.gestioneFlussi.domain.PercorsoFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PercorsoFileRepository extends JpaRepository<PercorsoFile, Long> {

	List<PercorsoFile> findByTipoFileAndPercorsoIsNotNull(String tipoFile);

	PercorsoFile findOneByTipoFileAndPercorsoIsNull(String tipoFile);

}
