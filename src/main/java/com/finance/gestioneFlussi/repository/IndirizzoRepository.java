package com.finance.gestioneFlussi.repository;

import com.finance.gestioneFlussi.domain.Indirizzo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long> {

}
