package com.finance.gestioneFlussi.repository;

import com.finance.gestioneFlussi.domain.MyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyEntityRepository extends JpaRepository<MyEntity, Object> {
}
