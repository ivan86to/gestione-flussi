package com.finance.gestioneFlussi.service.mapper;

import com.finance.gestioneFlussi.domain.Anagrafica;
import com.finance.gestioneFlussi.domain.FileCaricato;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.service.dto.AnagraficaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class AnagraficaMapper {

	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;

	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	public AnagraficaDTO settingsAnagraficaDTO(String[] elementi, FileCaricato fileCaricato) throws Exception {
		AnagraficaDTO _dto = new AnagraficaDTO();
		_dto.setIdEsterno(Long.valueOf(elementi[0]));
		_dto.setNome(elementi[1]);
		_dto.setCognome(elementi[2]);
		_dto.setCodiceFiscale(elementi[3]);
		_dto.setDataNascita(sdf.parse(elementi[4]));
		_dto.setFileCaricatoId(fileCaricato.getId());
		return _dto;
	}


	public List<Anagrafica> toEntityList(List<AnagraficaDTO> dtoList) {
		List<Anagrafica> entityList = new ArrayList<>();
		for (AnagraficaDTO dto : dtoList) {
			entityList.add(toEntity(dto));
		}
		return entityList;
	}

	public Anagrafica toEntity(AnagraficaDTO dto) {

		Anagrafica entity = new Anagrafica();
		entity.setId(dto.getId());
		entity.setIdEsterno(dto.getIdEsterno());
		entity.setNome(dto.getNome());
		entity.setCognome(dto.getCognome());
		entity.setCodiceFiscale(dto.getCodiceFiscale());
		entity.setDataNascita(dto.getDataNascita());
		entity.setFileCaricato(fileCaricatoRepository.findById(dto.getFileCaricatoId()).get());

		return entity;
	}

	public List<AnagraficaDTO> toDTOList(List<Anagrafica> entityList) {
		List<AnagraficaDTO> dtoList = new ArrayList<>();
		for (Anagrafica entity : entityList) {
			dtoList.add(toDTO(entity));
		}
		return dtoList;
	}

	public AnagraficaDTO toDTO(Anagrafica entity) {
		AnagraficaDTO dto = new AnagraficaDTO();
		dto.setId(entity.getId());
		dto.setIdEsterno(entity.getIdEsterno());
		dto.setNome(entity.getNome());
		dto.setCognome(entity.getCognome());
		dto.setCodiceFiscale(entity.getCodiceFiscale());
		dto.setDataNascita(entity.getDataNascita());
		dto.setFileCaricatoId(entity.getFileCaricato().getId());
		return dto;
	}

}

