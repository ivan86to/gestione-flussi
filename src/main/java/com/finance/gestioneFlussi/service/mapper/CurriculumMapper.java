package com.finance.gestioneFlussi.service.mapper;

import com.finance.gestioneFlussi.domain.Curriculum;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.repository.PercorsoFileRepository;
import com.finance.gestioneFlussi.service.dto.CurriculumDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurriculumMapper {

	@Autowired
	private PercorsoFileRepository percorsoFileRepository;
	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;

	public List<CurriculumDTO> toDTOList(List<Curriculum> entityList) {
		List<CurriculumDTO> dtoList = new ArrayList<>();
		CurriculumDTO dto = null;
		for (Curriculum entity : entityList) {
			if (dtoList.isEmpty()) {
				dtoList.add(toDTO(entity));
			} else {
				if (idPresente(dtoList, entity.getIdPersona())) {
					dto = caricaElementoDaLista(entity, dtoList);
					dto.getProfessioni().add(entity.getProfessione());
				} else {
					dtoList.add(toDTO(entity));
				}
			}
		}
		return dtoList;
	}

	public CurriculumDTO toDTO(Curriculum entity) {
		CurriculumDTO dto = new CurriculumDTO();
		dto.setIdPersona(entity.getIdPersona());
		List<String> professioni = new ArrayList<>();
		professioni.add(entity.getProfessione());
		dto.setProfessioni(professioni);
		return dto;
	}

	public List<Curriculum> toEntityList(List<CurriculumDTO> dtoList) {
		List<Curriculum> entityList = new ArrayList<>();
		for (CurriculumDTO dto : dtoList) {
			for (String professione : dto.getProfessioni()) {
				Curriculum entity = new Curriculum();
				entity.setIdPersona(dto.getIdPersona());
				entity.setProfessione(professione);
				entity.setFileCaricato(fileCaricatoRepository.findById(dto.getFileCaricatoId()).get());
				entityList.add(entity);
			}
		}
		return entityList;
	}

	public List<Curriculum> toEntity(CurriculumDTO dto) {
		List<Curriculum> curriculumList = new ArrayList<>();
		for (String professione : dto.getProfessioni()) {
			Curriculum entity = new Curriculum();
			entity.setIdPersona(dto.getIdPersona());
			entity.setProfessione(professione);
			entity.setFileCaricato(fileCaricatoRepository.findById(dto.getFileCaricatoId()).get());
			curriculumList.add(entity);
		}
		return curriculumList;
	}

	private boolean idPresente(List<CurriculumDTO> dtoList, Long idPersona) {
		for (CurriculumDTO dto : dtoList) {
			if (idPersona.equals(dto.getIdPersona())) {
				return true;
			}
		}
		return false;
	}

	private CurriculumDTO caricaElementoDaLista(Curriculum entity, List<CurriculumDTO> dtoList) {
		for (CurriculumDTO dto : dtoList) {
			if (entity.getIdPersona().equals(dto.getIdPersona())) {
				return dto;
			}
		}
		return null;
	}

}