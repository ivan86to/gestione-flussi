package com.finance.gestioneFlussi.service.mapper;

import com.finance.gestioneFlussi.domain.FileCaricato;
import com.finance.gestioneFlussi.domain.Indirizzo;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.service.dto.IndirizzoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IndirizziMapper {

	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;

	public IndirizzoDTO indirizziSettings(String line, FileCaricato fileCaricato) {
		IndirizzoDTO _indirizzo = new IndirizzoDTO();
		_indirizzo.setIdEsterno(Long.parseLong(line.substring(0, 7)));
		_indirizzo.setIndirizzo(line.substring(7, 57));
		_indirizzo.setCap(line.substring(57, 62));
		_indirizzo.setCitta(line.substring(62, 82));
		_indirizzo.setFileCaricatoId(fileCaricato.getId());
		return _indirizzo;
	}

	public List<Indirizzo> toEntityList(List<IndirizzoDTO> dtoList) {
		List<Indirizzo> entityList = new ArrayList<>();
		for (IndirizzoDTO dto : dtoList) {
			entityList.add(toEntity(dto));
		}
		return entityList;
	}

	public List<IndirizzoDTO> toDTOList(List<Indirizzo> indexList) {
		List<IndirizzoDTO> dto = new ArrayList<>();
		for (Indirizzo index : indexList) {
			dto.add(toDTO(index));
		}
		return dto;
	}

	public IndirizzoDTO toDTO(Indirizzo entity) {
		IndirizzoDTO dto = new IndirizzoDTO();
		dto.setFileCaricatoId(entity.getFileCaricato().getId());
		dto.setIndirizzo(entity.getIndirizzo());
		dto.setCitta(entity.getCitta());
		dto.setCap(entity.getCap());
		dto.setId(entity.getId());
		dto.setIdEsterno(entity.getIdEsterno());
		return dto;
	}

	public Indirizzo toEntity(IndirizzoDTO dto) {
		Indirizzo entity = new Indirizzo();
		entity.setIdEsterno(dto.getIdEsterno());
		entity.setIndirizzo(dto.getIndirizzo());
		entity.setCap(dto.getCap());
		entity.setCitta(dto.getCitta());
		entity.setFileCaricato(fileCaricatoRepository.findById(dto.getFileCaricatoId()).get());
		return entity;
	}

}
