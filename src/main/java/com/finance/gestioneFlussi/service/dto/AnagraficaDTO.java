package com.finance.gestioneFlussi.service.dto;

import java.util.Date;

public class AnagraficaDTO {

	private Long id;

	private Long idEsterno;

	private String nome;

	private String cognome;

	private String codiceFiscale;

	private Date dataNascita;

	private Long fileCaricatoId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEsterno() {
		return idEsterno;
	}

	public void setIdEsterno(Long idEsterno) {
		this.idEsterno = idEsterno;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public Long getFileCaricatoId() {
		return fileCaricatoId;
	}

	public void setFileCaricatoId(Long fileCaricatoId) {
		this.fileCaricatoId = fileCaricatoId;
	}
}
