package com.finance.gestioneFlussi.service.dto;

import java.util.List;

public class CurriculumDTO {

	private Long idPersona;

	private List<String> professioni;

	private Long fileCaricatoId;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<String> getProfessioni() {
		return professioni;
	}

	public void setProfessioni(List<String> professioni) {
		this.professioni = professioni;
	}

	public Long getFileCaricatoId() {
		return fileCaricatoId;
	}

	public void setFileCaricatoId(Long fileCaricatoId) {
		this.fileCaricatoId = fileCaricatoId;
	}
}
