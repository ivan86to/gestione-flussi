package com.finance.gestioneFlussi.service.dto;

public class IndirizzoDTO {

	private Long id;

	private Long idEsterno;

	private String indirizzo;

	private String cap;

	private String citta;

	private Long fileCaricatoId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEsterno() {
		return idEsterno;
	}

	public void setIdEsterno(Long idEsterno) {
		this.idEsterno = idEsterno;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public Long getFileCaricatoId() {
		return fileCaricatoId;
	}

	public void setFileCaricatoId(Long fileCaricatoId) {
		this.fileCaricatoId = fileCaricatoId;
	}
}
