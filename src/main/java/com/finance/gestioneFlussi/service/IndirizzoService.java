package com.finance.gestioneFlussi.service;

import com.finance.gestioneFlussi.domain.FileCaricato;
import com.finance.gestioneFlussi.domain.Indirizzo;
import com.finance.gestioneFlussi.domain.PercorsoFile;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.repository.IndirizzoRepository;
import com.finance.gestioneFlussi.repository.PercorsoFileRepository;
import com.finance.gestioneFlussi.service.dto.IndirizzoDTO;
import com.finance.gestioneFlussi.service.mapper.IndirizziMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class IndirizzoService {

	@Autowired
	private IndirizzoRepository indirizzoRepository;

	@Autowired
	private PercorsoFileRepository percorsoFileRepository;

	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;

	@Autowired
	private IndirizziMapper indirizziMapper;

	private final String TIPO_FILE = "indirizzi";

	public List<IndirizzoDTO> uploadIndirizziFromFile() throws Exception {
		List<IndirizzoDTO> indirizzi = new ArrayList<>();

		List<PercorsoFile> filePaths = percorsoFileRepository.findByTipoFileAndPercorsoIsNotNull(TIPO_FILE);
		for (PercorsoFile percorsoFile : filePaths) {
			String[] pathSplit = percorsoFile.getPercorso().split("\\\\");
			String pathDirectory = "";
			for (int i = 0; i < pathSplit.length - 1; i++) {
				pathDirectory += pathSplit[i] + "/";
			}
			File directoryBase = new File(pathDirectory);
			File[] listaFileInput = directoryBase.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String nomeFileAccettato = pathSplit[pathSplit.length - 1];
					String nome = nomeFileAccettato.split("\\*")[0];
					String ext = nomeFileAccettato.split("\\*")[1];
					return pathname.getName().startsWith(nome) && pathname.getName().endsWith(ext);
				}
			});
			for (File f : listaFileInput) {
				fileToDTO(f, indirizzi, percorsoFile, true);
			}
		}
		indirizzoRepository.saveAll(indirizziMapper.toEntityList(indirizzi));
		return indirizzi;
	}

	public IndirizzoDTO patchIndirizziFromFile(MultipartFile patch) throws Exception {
		File tempFile = convertToFile(patch);
		List<IndirizzoDTO> indirizzoDTOList = new ArrayList<>();
		PercorsoFile percorsoFile = percorsoFileRepository.findOneByTipoFileAndPercorsoIsNull(TIPO_FILE);
		FileCaricato fileCaricato = fileToDTO(tempFile, indirizzoDTOList, percorsoFile, false);

		if (dtoCorretto(indirizzoDTOList)) {

			fileCaricato = fileCaricatoRepository.save(fileCaricato);
			indirizzoDTOList.get(0).setFileCaricatoId(fileCaricato.getId());

			indirizzoRepository.save(indirizziMapper.toEntity(indirizzoDTOList.get(0)));
			tempFile.delete();
			return indirizzoDTOList.get(0);
		} else {
			tempFile.delete();
			return null;
		}
	}

	public List<IndirizzoDTO> listaDTO() {
		return indirizziMapper.toDTOList(indirizzoRepository.findAll());
	}

	public IndirizzoDTO getDTOByID(Long id) {
		Indirizzo indirizzo = indirizzoRepository.getOne(id);
		return indirizziMapper.toDTO(indirizzo);
	}

	public void delDTO(Long id) {
		indirizzoRepository.deleteById(id);
	}

	private File convertToFile(MultipartFile multipartFile) throws Exception {
		File tempFile = File.createTempFile("tempIndirizzi", null);
		tempFile.deleteOnExit();
		multipartFile.transferTo(tempFile);
		return tempFile;
	}

	private boolean dtoCorretto(List<IndirizzoDTO> indirizzoDTOList) {
		return indirizzoDTOList.size() == 1;
	}

	private FileCaricato fileToDTO(File file, final List<IndirizzoDTO> indirizzi, final PercorsoFile percorsoFile, boolean saveFileCaricato) throws Exception {
		FileCaricato fileCaricato = new FileCaricato();
		fileCaricato.setNomeFile(file.getAbsolutePath());
		fileCaricato.setDataCaricamento(Instant.now());
		fileCaricato.setPercorsoFile(percorsoFile);
		if (saveFileCaricato)
			fileCaricato = fileCaricatoRepository.save(fileCaricato);

		FileReader fileReader = new FileReader(file.getAbsolutePath());
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			indirizzi.add(indirizziMapper.indirizziSettings(line, fileCaricato));
		}
		bufferedReader.close();
		return fileCaricato;
	}
}

