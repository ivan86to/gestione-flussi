package com.finance.gestioneFlussi.service;

import com.finance.gestioneFlussi.domain.FileCaricato;
import com.finance.gestioneFlussi.domain.PercorsoFile;
import com.finance.gestioneFlussi.repository.AnagraficaRepository;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.repository.PercorsoFileRepository;
import com.finance.gestioneFlussi.service.dto.AnagraficaDTO;
import com.finance.gestioneFlussi.service.mapper.AnagraficaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class AnagraficaService {

	@Autowired
	private AnagraficaRepository anagraficaRepository;
	@Autowired
	private PercorsoFileRepository percorsoFileRepository;
	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;
	@Autowired
	private AnagraficaMapper anagraficaMapper;

	private final String TIPO_FILE = "anagrafica";

	public List<AnagraficaDTO> caricaFile() throws Exception {
		List<AnagraficaDTO> anagrafiche = new ArrayList<>();
		List<PercorsoFile> filePath = percorsoFileRepository.findByTipoFileAndPercorsoIsNotNull(TIPO_FILE);
		for (PercorsoFile percorsoFile : filePath) {
			String[] pathSplit = percorsoFile.getPercorso().split("\\\\");
			String path = "";
			for (int i = 0; i < pathSplit.length - 1; i++) {
				path += pathSplit[i] + "/";
			}
			File directoryBase = new File(path);
			File[] listaFileInput = directoryBase.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String nomeFileAccettato = pathSplit[pathSplit.length - 1];
					String nome = nomeFileAccettato.split("\\*")[0];
					String ext = nomeFileAccettato.split("\\*")[1];
					return pathname.getName().startsWith(nome) && pathname.getName().endsWith(ext);
				}
			});

			for (File file : listaFileInput) {
				fileToDTO(file, anagrafiche, percorsoFile, true);

			}
		}
		anagraficaRepository.saveAll(anagraficaMapper.toEntityList(anagrafiche));
		return anagrafiche;
	}

	private FileCaricato fileToDTO(File file, List<AnagraficaDTO> anagrafiche, PercorsoFile percorsoFile, boolean saveAnangrafica) throws Exception {
		FileCaricato fileCaricato = new FileCaricato();
		fileCaricato.setNomeFile(file.getAbsolutePath());
		fileCaricato.setDataCaricamento(Instant.now());
		fileCaricato.setPercorsoFile(percorsoFile);
		if (saveAnangrafica) {
			fileCaricato = fileCaricatoRepository.save(fileCaricato);
		}


		FileReader fileReader = new FileReader(file.getAbsolutePath());
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			anagrafiche.add(anagraficaMapper.settingsAnagraficaDTO(line.split(";"), fileCaricato));
		}
		bufferedReader.close();
		fileReader.close();

		return fileCaricato;
	}

	public AnagraficaDTO uploadFile(MultipartFile file) throws Exception {
		File tempFile = convertToFile(file);

		List<AnagraficaDTO> anagraficaDTOList = new ArrayList<>();
		PercorsoFile percorsoFile = percorsoFileRepository.findOneByTipoFileAndPercorsoIsNull(TIPO_FILE);
		FileCaricato fileCaricato = fileToDTO(tempFile, anagraficaDTOList, percorsoFile, false);

		if (dtoCorretto(anagraficaDTOList)) {

			fileCaricato = fileCaricatoRepository.save(fileCaricato);
			anagraficaDTOList.get(0).setFileCaricatoId(fileCaricato.getId());

			anagraficaRepository.save(
					anagraficaMapper.toEntity(anagraficaDTOList.get(0))
			);

			tempFile.delete();
			return anagraficaDTOList.get(0);
		} else {
			tempFile.delete();
			return null;
		}

	}

	private File convertToFile(MultipartFile multipartFile) throws Exception {
		File tempFile = File.createTempFile("tempAnagrafica", null);
		tempFile.deleteOnExit();
		multipartFile.transferTo(tempFile);
		return tempFile;
	}

	private boolean dtoCorretto(List<AnagraficaDTO> anagraficaDTOList) {
		return anagraficaDTOList.size() == 1;
	}

	public List<AnagraficaDTO> listaDTO() {
		return anagraficaMapper.toDTOList(anagraficaRepository.findAll());
	}

	public AnagraficaDTO getDTOByID(Long id) {
		return anagraficaMapper.toDTO(anagraficaRepository.getOne(id));
	}

	public void deleteById(Long id) {
		anagraficaRepository.deleteById(id);
	}

}





