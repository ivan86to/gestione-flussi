package com.finance.gestioneFlussi.service;

import com.finance.gestioneFlussi.domain.Curriculum;
import com.finance.gestioneFlussi.domain.FileCaricato;
import com.finance.gestioneFlussi.domain.PercorsoFile;
import com.finance.gestioneFlussi.repository.CurriculumRepository;
import com.finance.gestioneFlussi.repository.FileCaricatoRepository;
import com.finance.gestioneFlussi.repository.PercorsoFileRepository;
import com.finance.gestioneFlussi.service.dto.CurriculumDTO;
import com.finance.gestioneFlussi.service.mapper.CurriculumMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class CurriculumService {

	@Autowired
	private CurriculumRepository curriculumRepository;
	@Autowired
	private PercorsoFileRepository percorsoFileRepository;
	@Autowired
	private FileCaricatoRepository fileCaricatoRepository;
	@Autowired
	private CurriculumMapper curriculumMapper;

	private final String TIPO_FILE = "professioni";


	public List<CurriculumDTO> loadCurriculumFromFile() throws NumberFormatException, Exception {

		List<CurriculumDTO> curriculum = new ArrayList<CurriculumDTO>();

		List<PercorsoFile> filePaths = percorsoFileRepository.findByTipoFileAndPercorsoIsNotNull(TIPO_FILE);
		for (PercorsoFile percorsoFile : filePaths) {
			String pathDirectory = percorsoFile.getPercorso();
			String[] pathSplit = pathDirectory.split("\\\\");
			pathDirectory = "";
			for (int i = 0; i < pathSplit.length - 1; i++) {
				pathDirectory += pathSplit[i] + "/";
			}
			File directoryBase = new File(pathDirectory);

			File[] listaFileInput = directoryBase.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String nomeFileAccettato = pathSplit[pathSplit.length - 1];
					String nome = nomeFileAccettato.split("\\*")[0];
					String ext = nomeFileAccettato.split("\\*")[1];
					return pathname.getName().startsWith(nome) && pathname.getName().endsWith(ext);
				}
			});
			for (File f : listaFileInput) {
				fileToDTO(f, curriculum, percorsoFile, true);
			}
		}
		curriculumRepository.saveAll(curriculumMapper.toEntityList(curriculum));
		return curriculum;
	}

	public List<CurriculumDTO> listaDTO() {
		List<Curriculum> listaTemporanea = curriculumRepository.findAll();
		return curriculumMapper.toDTOList(listaTemporanea);
	}

	public CurriculumDTO getDTOByFile(MultipartFile multipartFile) throws Exception {
		File tempFile = convertToFile(multipartFile);

		List<CurriculumDTO> curriculumDTOList = new ArrayList<>();
		PercorsoFile percorsoFile = percorsoFileRepository.findOneByTipoFileAndPercorsoIsNull(TIPO_FILE);
		FileCaricato fileCaricato = fileToDTO(tempFile, curriculumDTOList, percorsoFile, false);

		if (dtoCorretto(curriculumDTOList)) {

			fileCaricato = fileCaricatoRepository.save(fileCaricato);
			curriculumDTOList.get(0).setFileCaricatoId(fileCaricato.getId());

			curriculumRepository.saveAll(
					curriculumMapper.toEntity(curriculumDTOList.get(0))
			);

			tempFile.delete();
			return curriculumDTOList.get(0);
		} else {
			tempFile.delete();
			return null;
		}
	}

	private boolean dtoCorretto(List<CurriculumDTO> curriculumDTOList) {
		return curriculumDTOList.size() == 1 && curriculumDTOList.get(0).getProfessioni().size() == 1;
	}

	private File convertToFile(MultipartFile multipartFile) throws Exception {
		File tempFile = File.createTempFile("tempCurriculum", null);
		tempFile.deleteOnExit();
		multipartFile.transferTo(tempFile);
		return tempFile;
	}

	private FileCaricato fileToDTO(File file, final List<CurriculumDTO> curriculum, final PercorsoFile percorsoFile, boolean saveFileCaricato) throws Exception {
		FileCaricato fileCaricato = new FileCaricato();
		fileCaricato.setNomeFile(file.getAbsolutePath());
		fileCaricato.setDataCaricamento(Instant.now());
		fileCaricato.setPercorsoFile(percorsoFile);
		if (saveFileCaricato) {
			fileCaricato = fileCaricatoRepository.save(fileCaricato);
		}

		FileReader fileReader = new FileReader(file.getAbsolutePath());
		BufferedReader reader = new BufferedReader(fileReader);
		CurriculumDTO dto;
		List<String> professioni = null;
		String line;
		while ((line = reader.readLine()) != null) {
			if ((line.startsWith("ID"))) {
				Long id = Long.parseLong(line.substring(2));
				dto = new CurriculumDTO();
				professioni = new ArrayList<String>();
				dto.setProfessioni(professioni);
				dto.setIdPersona(id);
				dto.setFileCaricatoId(fileCaricato.getId());
				curriculum.add(dto);
			} else {
				professioni.add(line);
			}
		}
		reader.close();
		fileReader.close();
		return fileCaricato;
	}

	public CurriculumDTO getDTOByID(Long id) {
		return curriculumMapper.toDTO(curriculumRepository.getOne(id));
	}

	public void deleteByID(Long id) {
		curriculumRepository.deleteById(id);
	}
}


