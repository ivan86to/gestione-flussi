package com.finance.gestioneFlussi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneFlussiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneFlussiApplication.class, args);
	}

}
